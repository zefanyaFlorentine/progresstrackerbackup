const Pool = require('pg').Pool
const crypto = require('crypto')
const nodeMailer = require('nodemailer')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const SECRET_KEY = "secretkey23456";

// Pool
const pool = new Pool({
    user: 'lklbgvkupczzpn',
    host: 'ec2-54-221-238-248.compute-1.amazonaws.com',
    database: 'd5geph93pfv410',
    password: '899bcdcce6723f2dba5f638cfdc8dfd23c2b5965c1080059a6add5829fe39c48',
    port: 5432,
    ssl: true
})

const adminShowListProjects = (request, response) => {
    pool.query('SELECT project_name AS title, project_id AS id FROM projects WHERE project_id > 0', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const adminShowListUsers = (request, response) => {
    pool.query('SELECT name AS title, user_id AS id, email FROM users WHERE user_id > 0 AND isadmin = false', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

  
const adminShowListRoles = (request, response) => {
    pool.query('SELECT role_name AS title, role_id AS id FROM roles WHERE role_id > 0', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const adminAddProject = (request, response) => {
    const projectName = request.body.name
    pool.query('INSERT INTO projects(project_name) VALUES($1)', [projectName], (error, results) => {
        if (error) {
            response.status(200).json({"code": 201, "message": "failed"})
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

const adminAddRole = (request, response) => {
    const roleName = request.body.name
    pool.query('INSERT INTO roles(role_name) VALUES($1)', [roleName], (error, results) => {
        if (error) {
            response.status(200).json({"code": 201, "message": "error"})
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

const adminAddUser = (request, response) => { 
  const name = request.body.name
  const email= request.body.email
    var length = 8,
    charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }

        //Proses kirim ke email
       let transporter = nodeMailer.createTransport({
          host: 'smtp.gmail.com',
          port: 465,
          secure: true,
          auth: {
              user: 'tematik19@gmail.com',
              pass: 'iniuntuktemat1k'
          }
      });

      let mailOptions = {
          from: '"Tematik" <tematik19@gmail.com>', // sender address
          to: email, // list of receivers
          subject: "Registrasi Account baru Progress Tracker", // Subject line
          text: "ini password kamu : " + retVal, // plain text body
          html: 'Ini password Anda : ' + retVal + "<hr/>" + 'Jangan bagikan password ini ke siapapun. Terima Kasih.' // html body
      };

      console.log("password => ", mailOptions.text);
      transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
              return console.log(error);
          }
          console.log('Message %s sent: %s', info.messageId, info.response);
              res.render('index');
          });


    var hash = crypto.createHash('md5').update(retVal).digest('hex');
    console.log(hash);

    pool.query('INSERT INTO users(name,email,isAdmin,password) VALUES($1,$2,FALSE,$3)', [name,email,hash], (error, results) => {
        if (error) {
            response.status(200).json({"code": 201, "message": "failed"})
            return;
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}


const adminDeleteUser = (request, response) => {
    const id = request.body.id
    console.log(id);

    pool.query('DELETE FROM report WHERE user_id = $1', [id], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
    })

    pool.query('DELETE FROM users WHERE user_id = $1', [id], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
    })

    response.status(200).json({"code": 200, "message": "success"})
}

const adminDeleteRole = (request, response) => {
    const id = request.body.id
    console.log(id);

    pool.query('DELETE FROM report WHERE role_id = $1', [id], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
    })

    pool.query('DELETE FROM roles WHERE role_id = $1', [id], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
    })

    response.status(200).json({"code": 200, "message": "success"})
}

const adminDeleteProject = (request, response) => {
    const id = request.body.id
    console.log(id);

    pool.query('DELETE FROM report WHERE project_id = $1', [id], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
    })

    pool.query('DELETE FROM projects WHERE project_id = $1', [id], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
    })

    response.status(200).json({"code": 200, "message": "success"})
}

const adminEditUser = (request, response) => {
    const id = request.body.id
    const name = request.body.name
    const email = request.body.email
    pool.query('UPDATE users SET name = $1, email = $2 WHERE user_id = $3', [name,email, id], (error, results) => {
        if (error) {
            response.status(200).json({"code": 201, "message": "failed"})
            return;
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

const adminEditRole = (request, response) => {
    const id = request.body.id
    const name = request.body.name
    pool.query('UPDATE roles SET role_name = $1 WHERE role_id = $2', [name,id], (error, results) => {
        if (error) {
            response.status(200).json({"code": 201, "message": "failed"})
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

const adminEditProject = (request, response) => {
    const id = request.body.id
    const name = request.body.name
    pool.query('UPDATE projects SET project_name = $1 WHERE project_id = $2', [name,id], (error, results) => {
        if (error) {
            response.status(200).json({"code": 201, "message": "failed"})
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

const adminSearchProject  = (request, response) => {
    const search = '%' + request.body.search + '%'
    pool.query('SELECT project_name AS title, project_id AS id FROM projects WHERE LOWER(project_name) LIKE LOWER($1) AND project_id > 0', [search] ,(error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const adminSearchUser  = (request, response) => {
    const search = '%' + request.body.search + '%'
    pool.query('SELECT name AS title, user_id AS id FROM users WHERE LOWER(name) LIKE LOWER($1) AND user_id > 0', [search] ,(error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const adminSearchRole  = (request, response) => {
    const search = '%' + request.body.search + '%'
    pool.query('SELECT role_name AS title, role_id AS id FROM roles WHERE LOWER(role_name) LIKE LOWER($1) AND role_id > 0', [search] ,(error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const login = (request, response) => {
    // response.status(200).send({ access_token: '' });
    console.log("MASUK BACKEND")
    const email =  request.body.email
    console.log("email di backend => ",email )
    const password =  crypto.createHash('md5').update(request.body.password).digest('hex')
    // const password = request.body.password;

        console.log("ini email ", email)
        pool.query('select * from users where email = $1', [email], (error, results) => {

            if(results.rows.length < 1) {
                console.log("email tidak terdaftar !");
                return response.send({"message":'User not found!', code: 404});
            }

        console.log("results => ", results)
        console.log("ini password ", password)
        console.log("ini pass dari DB ", results.rows[0].password)
        console.log(results.rows)

        // var hash_password = bcrypt.hashSync(results.rows[0].password, 10)

        if (error) {
            response.status(500).send('Server error!') 
            return;
        }
        console.log("error lewat")
        // if(!results) {
        //     response.status(404).send('User not found!')
        //     return;
        // }
        // console.log("!results lewat")
        const result = bcrypt.compareSync(password , bcrypt.hashSync(results.rows[0].password, 10))
        console.log("const result lewat")
        if(!result) { 
            response.status(401).send('Password not valid!')
            return;
        }
        else console.log("Password benar");
        if(results.rows[0].isadmin === false) { 
            console.log("Admin status => ",results.rows[0].isadmin)
            console.log("This Account is not an Admin !")
            response.status(401).send('This page is for admin only !')
            return;
        } else console.log("This is an Admin !");

        const expiresIn = 24 * 60 * 60;
        const accessToken = jwt.sign({ 
            id: results.rows }, 
            SECRET_KEY, {
            expiresIn: expiresIn
        });
        console.log("JWT Token => ",accessToken)
        return response.status(200).send({ "access_token": accessToken, "expires_in": expiresIn, status: "success", code: 200, "slug" : crypto.createHash('md5').update(results.rows[0].name).digest('hex')});
    });
}

const adminDetailProject1  = (request, response) => {
    var resultDataAll = []
    const id = request.body.id
    const startDate = request.body.date1
    const endDate = request.body.date2
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    pool.query('SELECT array_agg(DISTINCT(ro.role_name)) AS name, r.created_date AS tgl FROM report AS r, roles as ro WHERE r.role_id > 0 AND r.user_id > 0 AND r.project_id > 0 AND r.task_id > 0 AND r.role_id = ro.role_id AND r.project_id = $1 AND r.created_date >= $2 AND r.created_date <= $3 GROUP BY r.created_date ORDER BY r.created_date ASC',
                [id,startDate,endDate] ,(error, results) => {

        if (error) {
            throw error
        }
        if(results){
            if(results.rows.length > 0){
                for(let x = 0 ; x<results.rows.length ; x++){
                    var tglTemp = results.rows[x].tgl + ''
                    var tglfix = tglTemp.split(' ')
                    var tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
                    var parts1 = tempDate.split('-')
                    var date1 = new Date(parts1[0], parts1[1], parts1[2])
                    date1.setMonth(date1.getMonth()-1)
                    date1.setDate(date1.getDate()+1)
                    console.log(date1)
                    var dayName = days[date1.getDay()-1] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[date1.getMonth()] + ' ' + tglfix[3]; 
                    
                    var name = []
                    var y = 0
                    while(1){
                        if(results.rows[x].name[y]) {
                            name.push(results.rows[x].name[y])
                        } else { break }
                        y += 1;
                    }

                    resultDataAll.push({
                        cardTitle: dayName,
                        cardList: name,
                        cardDate: tempDate,
                        cardId: id
                    })
                }
            }
        }
        response.status(200).json(resultDataAll);
    })
}

const adminDetailRole1  = (request, response) => {
    var resultDataAll = []
    const id = request.body.id
    const startDate = request.body.date1
    const endDate = request.body.date2
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    pool.query('SELECT array_agg(DISTINCT(p.project_name)) AS name, r.created_date AS tgl FROM report AS r, projects as p WHERE r.role_id > 0 AND r.user_id > 0 AND r.project_id > 0 AND r.task_id > 0 AND r.project_id = p.project_id AND r.role_id = $1 AND r.created_date >= $2 AND r.created_date <= $3 GROUP BY r.created_date ORDER BY r.created_date ASC',
                [id,startDate,endDate] ,(error, results) => {

        if (error) {
            throw error
        }
        if(results){
            if(results.rows.length > 0){
                for(let x = 0 ; x<results.rows.length ; x++){
                    var tglTemp = results.rows[x].tgl + ''
                    var tglfix = tglTemp.split(' ')
                    var tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
                    var parts1 = tempDate.split('-')
                    var date1 = new Date(parts1[0], parts1[1], parts1[2])
                    date1.setMonth(date1.getMonth()-1)
                    date1.setDate(date1.getDate()+1)
                    console.log(date1)
                    var dayName = days[date1.getDay()-1] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[date1.getMonth()] + ' ' + tglfix[3]; 
                    
                    var name = []
                    var y = 0
                    while(1){
                        if(results.rows[x].name[y]) {
                            name.push(results.rows[x].name[y])
                        } else { break }
                        y += 1;
                    }

                    resultDataAll.push({
                        cardTitle: dayName,
                        cardList: name,
                        cardDate: tempDate,
                        cardId: id
                    })
                }
            }
        }
        response.status(200).json(resultDataAll);
    })
}

const adminDetailUser1  = (request, response) => {
    var resultDataAll = []
    const id = request.body.id
    const startDate = request.body.date1
    const endDate = request.body.date2
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    pool.query('SELECT array_agg(DISTINCT(p.project_name)) AS name, r.created_date AS tgl FROM report AS r, projects as p WHERE r.role_id > 0 AND r.user_id > 0 AND r.project_id > 0 AND r.task_id > 0 AND r.project_id = p.project_id AND r.user_id = $1 AND r.created_date >= $2 AND r.created_date <= $3 GROUP BY r.created_date ORDER BY r.created_date ASC',
                [id,startDate,endDate] ,(error, results) => {

        if (error) {
            throw error
        }
        if(results){
            if(results.rows.length > 0){
                for(let x = 0 ; x<results.rows.length ; x++){
                    var tglTemp = results.rows[x].tgl + ''
                    var tglfix = tglTemp.split(' ')
                    var tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
                    var parts1 = tempDate.split('-')
                    var date1 = new Date(parts1[0], parts1[1], parts1[2])
                    date1.setMonth(date1.getMonth()-1)
                    date1.setDate(date1.getDate()+1)
                    console.log(date1)
                    var dayName = days[date1.getDay()-1] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[date1.getMonth()] + ' ' + tglfix[3]; 
                    
                    var name = []
                    var y = 0
                    while(1){
                        if(results.rows[x].name[y]) {
                            name.push(results.rows[x].name[y])
                        } else { break }
                        y += 1;
                    }

                    resultDataAll.push({
                        cardTitle: dayName,
                        cardList: name,
                        cardDate: tempDate,
                        cardId: id
                    })
                }
            }
        }
        response.status(200).json(resultDataAll);
    })
}

const adminDetailProject2  = (request, response) => {
    var resultDataAll = []
    const id = request.body.id
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    pool.query('SELECT array_agg(DISTINCT(ro.role_name)) AS name, r.created_date AS tgl FROM report AS r, roles as ro WHERE r.role_id > 0 AND r.user_id > 0 AND r.project_id > 0 AND r.task_id > 0 AND r.role_id = ro.role_id AND r.project_id = $1 GROUP BY r.created_date ORDER BY r.created_date ASC',
                [id] ,(error, results) => {

        if (error) {
            throw error
        }
        if(results){
            if(results.rows.length > 0){
                for(let x = 0 ; x<results.rows.length ; x++){
                    var tglTemp = results.rows[x].tgl + ''
                    var tglfix = tglTemp.split(' ')
                    var tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
                    var parts1 = tempDate.split('-')
                    var date1 = new Date(parts1[0], parts1[1], parts1[2])
                    date1.setMonth(date1.getMonth()-1)
                    date1.setDate(date1.getDate()+1)
                    console.log(date1)
                    var dayName = days[date1.getDay()-1] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[date1.getMonth()] + ' ' + tglfix[3]; 
                    
                    var name = []
                    var y = 0
                    while(1){
                        if(results.rows[x].name[y]) {
                            name.push(results.rows[x].name[y])
                        } else { break }
                        y += 1;
                    }

                    resultDataAll.push({
                        cardTitle: dayName,
                        cardList: name,
                        cardDate: tempDate,
                        cardId: id
                    })
                }
            }
        }
        response.status(200).json(resultDataAll);
    })
}

const adminDetailRole2  = (request, response) => {
    var resultDataAll = []
    const id = request.body.id
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    pool.query('SELECT array_agg(DISTINCT(p.project_name)) AS name, r.created_date AS tgl FROM report AS r, projects as p WHERE r.role_id > 0 AND r.user_id > 0 AND r.project_id > 0 AND r.task_id > 0 AND r.project_id = p.project_id AND r.role_id = $1 GROUP BY r.created_date ORDER BY r.created_date ASC',
                [id] ,(error, results) => {

        if (error) {
            throw error
        }
        if(results){
            if(results.rows.length > 0){
                for(let x = 0 ; x<results.rows.length ; x++){
                    var tglTemp = results.rows[x].tgl + ''
                    var tglfix = tglTemp.split(' ')
                    var tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
                    var parts1 = tempDate.split('-')
                    var date1 = new Date(parts1[0], parts1[1], parts1[2])
                    date1.setMonth(date1.getMonth()-1)
                    date1.setDate(date1.getDate()+1)
                    console.log(date1)
                    var dayName = days[date1.getDay()-1] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[date1.getMonth()] + ' ' + tglfix[3]; 
                    
                    var name = []
                    var y = 0
                    while(1){
                        if(results.rows[x].name[y]) {
                            name.push(results.rows[x].name[y])
                        } else { break }
                        y += 1;
                    }

                    resultDataAll.push({
                        cardTitle: dayName,
                        cardList: name,
                        cardDate: tempDate,
                        cardId: id
                    })
                }
            }
        }
        response.status(200).json(resultDataAll);
    })
}

const adminDetailUser2  = (request, response) => {
    var resultDataAll = []
    const id = request.body.id
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    pool.query('SELECT array_agg(DISTINCT(p.project_name)) AS name, r.created_date AS tgl FROM report AS r, projects as p WHERE r.role_id > 0 AND r.user_id > 0 AND r.project_id > 0 AND r.task_id > 0 AND r.project_id = p.project_id AND r.user_id = $1 GROUP BY r.created_date ORDER BY r.created_date ASC',
                [id] ,(error, results) => {

        if (error) {
            throw error
        }
        if(results){
            if(results.rows.length > 0){
                for(let x = 0 ; x<results.rows.length ; x++){
                    var tglTemp = results.rows[x].tgl + ''
                    var tglfix = tglTemp.split(' ')
                    var tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
                    var parts1 = tempDate.split('-')
                    var date1 = new Date(parts1[0], parts1[1], parts1[2])
                    date1.setMonth(date1.getMonth()-1)
                    date1.setDate(date1.getDate()+1)
                    console.log(date1)
                    var dayName = days[date1.getDay()-1] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[date1.getMonth()] + ' ' + tglfix[3]; 
                    
                    var name = []
                    var y = 0
                    while(1){
                        if(results.rows[x].name[y]) {
                            name.push(results.rows[x].name[y])
                        } else { break }
                        y += 1;
                    }

                    resultDataAll.push({
                        cardTitle: dayName,
                        cardList: name,
                        cardDate: tempDate,
                        cardId: id
                    })
                }
            }
        }
        response.status(200).json(resultDataAll);
    })
}


const reportDetailByProject = (request, response) => {
    const projectId = request.body.projectId
    const created_date = request.body.created_date
    
    console.log("ini id project =>",projectId)

    pool.query('select u.name, u.user_id, ro.role_name, ro.role_id, p.project_id, COUNT(t.task_id) as total_task,  r.created_date from report r, roles ro, users u , projects p, task t where u.user_id = r.user_id AND ro.role_id = r.role_id AND t.task_id = r.task_id AND r.project_id = p.project_id AND r.project_id = $1 AND r.created_date = $2 GROUP BY u.name, u.user_id, ro.role_name, ro.role_id, p.project_id, r.created_date', [projectId, created_date],  (error, results) => {
        if(error){
            throw error   
        }
        if(results){
            // data.push({
            //     result: results.rows,
            //     project_id : projectId,
            //     role_id : results.rows.roleID
            //     })
            response.status(200).json(results.rows)
            console.log("ini result detail => ",results.rows)
        }
    })
}

const reportDetailByUser = (request, response) => {
    const userId = request.body.userId
    const created_date = request.body.created_date
    
    console.log("ini id user =>", userId)

    pool.query('select ro.role_name, ro.role_id, p.project_id, p.project_name, COUNT(t.task_id) as total_task,  r.created_date from report r, roles ro, users u , projects p, task t where u.user_id = r.user_id AND r.user_id = $1 AND ro.role_id = r.role_id AND t.task_id = r.task_id AND r.project_id = p.project_id AND r.created_date = $2 GROUP BY u.name, u.user_id, ro.role_name, ro.role_id, p.project_id, r.created_date', [userId, created_date],  (error, results) => {
        if(error){
            throw error   
        }
        if(results){
            // data.push({
            //     result: results.rows,
            //     project_id : projectId,
            //     role_id : results.rows.roleID
            //     })
            response.status(200).json(results.rows)
            console.log("ini result detail => ",results.rows)
        }
    })
}

const taskList = (request, response) => {
    const roleId = request.body.roleId
    const projectId = request.body.projectId
    const userId = request.body.userId
    const createdDate = request.body.created_date

    console.log("ID role => ",roleId)
    console.log("ID project =>",projectId)
    console.log("ID role => ", userId)
    console.log("ID createdDate =>", createdDate)

    pool.query('select t.task_name, t.task_description, t.progress from report r, task t where r.project_id = $1 AND r.user_id = $2 AND r.role_id = $3 AND r.task_id = t.task_id AND r.created_date = $4', [projectId, userId, roleId, createdDate], (error, results) => {
        if (error) {
            throw error
        }
        if (results) {
            console.log("ini result detail => ", results.rows)
            response.status(200).json(results.rows)
        }
    })
}

const reportDetailByRole = (request, response) => {
    const roleId = request.body.roleId
    const created_date = request.body.created_date
    
    console.log("ini id role =>",roleId)

    pool.query('select u.name, u.user_id, p.project_name, ro.role_id, p.project_id, COUNT(t.task_id) as total_task,  r.created_date from report r, roles ro, users u , projects p, task t where u.user_id = r.user_id AND ro.role_id = r.role_id AND t.task_id = r.task_id AND r.project_id = p.project_id AND r.role_id = $1 AND r.created_date = $2 GROUP BY u.name, u.user_id, ro.role_name, ro.role_id, r.created_date, p.project_name, p.project_id', [roleId, created_date],  (error, results) => {
        if(error){
            throw error   
        }
        if(results){
            // data.push({
            //     result: results.rows,
            //     project_id : projectId,
            //     role_id : results.rows.roleID
            //     })
            response.status(200).json(results.rows)
            console.log("ini result detail => ",results.rows)
        }
    })
}

const deleteReport1 = (request, response) => {
    const userId = request.body.id
    const reportDate = request.body.reportDate

    pool.query('DELETE FROM report WHERE user_id = $1 AND created_date = $2', [userId,reportDate], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

const deleteReport2 = (request, response) => {
    const projectId = request.body.id
    const reportDate = request.body.reportDate

    pool.query('DELETE FROM report WHERE project_id = $1 AND created_date = $2', [projectId,reportDate], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

const deleteReport3 = (request, response) => {
    const roleId = request.body.id
    const reportDate = request.body.reportDate

    pool.query('DELETE FROM report WHERE role_id = $1 AND created_date = $2', [roleId,reportDate], (error, results) => {
        if(error){
            response.status(200).json({"code": 201, "message": "failed"})
        }
        response.status(200).json({"code": 200, "message": "success"})
    })
}

module.exports = {
    adminShowListProjects,
    adminShowListUsers,
    adminShowListRoles,
    adminAddProject,
    adminAddRole,
    adminAddUser,
    adminDeleteUser,
    adminEditUser,
    adminSearchProject,
    adminSearchUser,
    adminSearchRole,
    adminDetailProject1,
    adminDetailRole1,
    adminDetailUser1,
    adminDetailProject2,
    adminDetailRole2,
    adminDetailUser2,
    login,
    reportDetailByProject,
    taskList,
    reportDetailByRole,
    adminDeleteProject,
    adminDeleteRole,
    adminEditProject,
    adminEditRole,
    deleteReport1,
    deleteReport2,
    deleteReport3,
    reportDetailByUser
}