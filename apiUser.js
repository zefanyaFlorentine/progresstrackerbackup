const Pool = require('pg').Pool
const crypto = require('crypto')
const nodeMailer = require('nodemailer')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

// var async = require('async');
const SECRET_KEY = "secretkey23456";

const pool = new Pool({
  user: 'lklbgvkupczzpn',
  host: 'ec2-54-221-238-248.compute-1.amazonaws.com',
  database: 'd5geph93pfv410',
  password: '899bcdcce6723f2dba5f638cfdc8dfd23c2b5965c1080059a6add5829fe39c48',
  port: 5432,
  ssl: true
})

  const login = (request, response) => {
    // response.status(200).send({ access_token: '' });
    console.log("MASUK BACKEND")
    const email =  request.body.email
    console.log("email di backend => ",email )
    const password =  crypto.createHash('md5').update(request.body.password).digest('hex')
    // const password = request.body.password;

        console.log("ini email ", email)
        pool.query('select * from users where email = $1', [email], (error, results) => {
        
          if(results.rows.length < 1) {
            console.log("email tidak terdaftar !");
            return response.send({"message":'User not found!', code: 404});
          }
        console.log("results => ", results.rows)
        console.log("ini password ", password)
        // console.log("ini pass dari DB ", results.rows[0].password)
        // console.log(results.rows)

        // var hash_password = bcrypt.hashSync(results.rows[0].password, 10)

        if (error) return response.status(500).send('Server error!') 
        console.log("error lewat")
        // if(!results) return response.status(404).send('User not found!')
        // console.log("!results lewat")
        const result = bcrypt.compareSync(password, bcrypt.hashSync(results.rows[0].password, 10))
        console.log("const result lewat")
        if(!result) {return response.status(401).send('Password not valid!'); }
        else console.log("Password benar");
        if(results.rows[0].isadmin === true) { 
            console.log("Admin status => ",results.rows[0].isadmin)
            console.log("Admin shouldn't be logged in here")
            return response.status(401).send('Please login in the Admin Login Page !');
        } else console.log("This is an ordinary user !");

        const expiresIn = 24 * 60 * 60;
        const accessToken = jwt.sign({ 
            id: results.rows }, 
            SECRET_KEY, {
            expiresIn: expiresIn
        });
        console.log("JWT Token => ",accessToken)
        return response.status(200).send({ "access_token": accessToken, "expires_in": expiresIn, status: "success", code: 200});
    });
}


const getAllRoles = (request, response) => {
  pool.query('SELECT role_name AS roleName, role_id AS roleId FROM roles WHERE role_id > 0', (error, results) => {
      if (error) {
          throw error
      }
      response.status(200).json(results.rows)
  })
}

const getAllProjects = (request, response) => {
  pool.query('SELECT project_name AS projectName, project_id AS projectId FROM projects WHERE project_id > 0', (error, results) => {
      if (error) {
          throw error
      }
      response.status(200).json(results.rows)
  })
}

const dashboardAll  = (request, response) => {
  var resultDataAll = []
  var taskData = []
  var finalData = []
  var finalList = []
  var reason = false
  const id = request.body.id
  var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  
  pool.query('SELECT r.have_reason AS reason, p.project_name AS projectname, ro.role_name AS rolename, array_agg(t.task_name) AS taskname, array_agg(t.progress) AS taskprogress, r.created_date AS tgl FROM report AS r, projects as p, task AS t, roles AS ro WHERE r.project_id = p.project_id AND r.task_id = t.task_id AND r.role_id = ro.role_id AND r.user_id = $1 GROUP BY r.have_reason, r.created_date, p.project_name, ro.role_name, r.role_id ORDER BY r.created_date ASC, p.project_name ASC',
              [id] ,(error, results) => {
      if (error) {
          return error
      }
      if(results){
          if(results.rows.length > 0){
              var dayName = null
              var tempDate = null
              
              for(let x = 0 ; x<results.rows.length ; x++){
                if((x>0 && x!=results.rows.length-1 && results.rows[x].projectname!=results.rows[x-1].projectname) || (x>0 && results.rows[x].rolename!=results.rows[x-1].rolename) || (x>0 && results.rows[x].tgl!=results.rows[x-1].tgl)){
                  resultDataAll.push({
                    cardTitle: dayName,
                    cardList: {
                      projectName: results.rows[x-1].projectname,
                      listRoles: {
                        roleName: results.rows[x-1].rolename,
                        listTasks: taskData 
                      }
                    },
                    cardDate: tempDate,
                    cardId: id,
                    haveReason: reason, 
                  })
                  taskData = []

                }
            
                taskData.push({
                  taskName: results.rows[x].taskname,
                  taskProgress: results.rows[x].taskprogress
                })          
                
                var tglTemp = results.rows[x].tgl + ''
                var tglfix = tglTemp.split(' ')
                tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
                var parts1 = tempDate.split('-')
                var date1 = new Date(parts1[0], parts1[1], parts1[2])
                date1.setMonth(date1.getMonth()-1)
                date1.setDate(date1.getDate()+1)
                var dayArray = date1.getDay() - 1
                var monthArray = date1.getMonth()
                if(dayArray < 0){
                  dayArray = 6
                }
                if(monthArray < 0){
                  monthArray = 11
                }
                reason = results.rows[x].reason
                dayName = days[dayArray] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[monthArray] + ' ' + tglfix[3]; 
                
                if(x==results.rows.length-1){
                  resultDataAll.push({
                    cardTitle: dayName,
                    cardList: {
                      projectName: results.rows[x].projectname,
                      listRoles: {
                        roleName: results.rows[x].rolename,
                        listTasks: taskData 
                      }
                    },
                    cardDate: tempDate,
                    cardId: id,
                    haveReason: reason, 
                  })
                  taskData = []   
                }
              }

              for(z=0 ; z<resultDataAll.length ; z++){
                if(z>0 && resultDataAll[z].cardTitle!=resultDataAll[z-1].cardTitle){
                  finalData.push({
                    cardTitle: resultDataAll[z-1].cardTitle,
                    cardBody: finalList,
                    cardDate: resultDataAll[z-1].cardDate,
                    cardId: resultDataAll[z-1].cardId,
                    status: "full",
                    haveReason: resultDataAll[z-1].haveReason,
                  })
                  finalList = []
                }
                if(z==resultDataAll.length-1){
                  finalList.push({
                    projectName: resultDataAll[z].cardList.projectName,
                    roleName: resultDataAll[z].cardList.listRoles.roleName,
                    taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
                    taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
                  })
                  finalData.push({
                    cardTitle: resultDataAll[z].cardTitle,
                    cardBody: finalList,
                    cardDate: resultDataAll[z].cardDate,
                    cardId: resultDataAll[z].cardId,
                    status: "full",
                    haveReason: resultDataAll[z].haveReason,
                  })
                  finalList = []
                }
                finalList.push({
                  projectName: resultDataAll[z].cardList.projectName,
                  roleName: resultDataAll[z].cardList.listRoles.roleName,
                  taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
                  taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
                })
              }
          } else {
            response.status(200).json(results.rows)
            return
          }
      }

      var tglAkhirTemp = new Date()
      var tglAkhir = tglAkhirTemp.getFullYear() + '-' +  ('0' + (tglAkhirTemp.getMonth()+1)).slice(-2) + '-' + ('0' + (tglAkhirTemp.getDate())).slice(-2)
      var finalData2 = []
      pool.query('SELECT created_date AS tglawal FROM report WHERE user_id = $1 ORDER BY created_date ASC LIMIT 1', [id], (error, results) => {
        if(error){
          return error
        }
        console.log(results.rows + ' ' + id)
        var tglTemp2 = results.rows[0].tglawal + ''
        var tglfix2 = tglTemp2.split(' ')
        var tglAwal = tglfix2[3] + '-' + ('0' + (monthIdx.indexOf(tglfix2[1])+1)).slice(-2) + '-' + ('0' + (tglfix2[2])).slice(-2)
        var jmlHari = 0
        console.log(tglAwal)
        var dateTemp = ''
        var tglCheck = Number(tglfix2[2])
        var bulanCheck = monthIdx.indexOf(tglfix2[1]) + 1
        var tahunCheck = Number(tglfix2[3])
        var idx = 0
        var flagArray = 0
        var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];

        while(true){
          if(idx == 0){
            jmlHari = new Date(tahunCheck, bulanCheck, 0).getDate()
          } else if (tglCheck > jmlHari && idx > 0){
            tglCheck = 1;
          }
          if(tglCheck == 1 && idx > 0){
            bulanCheck += 1;
            jmlHari = new Date(tahunCheck, bulanCheck, 0).getDate()
          }
          if(bulanCheck > 12 && idx > 0){
            bulanCheck = 1
            tahunCheck += 1
          }

          tglAwal = tahunCheck + '-' + ('0' + (bulanCheck)).slice(-2) + '-' + ('0' + (tglCheck)).slice(-2)
          dateTemp = new Date(tahunCheck, bulanCheck, tglCheck)
          dateTemp.setMonth(dateTemp.getMonth()-1)
          dateTemp.setDate(dateTemp.getDate()+1)
          var dayArray = dateTemp.getDay() - 1
          var monthArray = bulanCheck - 1
          if(dayArray < 0){
            dayArray = 6
          }
          if(monthArray < 0){
            monthArray = 11
          }
          
          
          tglAwalTag = days[dayArray] + ', ' + ('0' + (tglCheck)).slice(-2) + ' ' + months[monthArray] + ' ' + tahunCheck;

          if(flagArray == 99) {
            finalData2.push({
              cardTitle: tglAwalTag,
              cardBody: "",
              cardDate: tglAwal,
              cardId: "",
              status: "empty",
              haveReason: false,
            })
            if(tglAkhir === tglAwal){
              break;
            }
          } else if(flagArray == 0){ 
            if(finalData[idx].cardDate == tglAwal){
              finalData2.push(finalData[idx])
              idx+=1
            } else if (finalData[idx].cardDate != tglAwal){
              if(tglAkhir === tglAwal){
                break;
              }
              finalData2.push({
                cardTitle: tglAwalTag,
                cardBody: "",
                cardDate: tglAwal,
                cardId: "",
                status: "empty",
                haveReason: false,
              })
            }
            if(tglAkhir === tglAwal){
              break;
            } 
          }

          tglCheck+=1
          if(idx >= finalData.length){
            flagArray = 99
          }
        }

        response.status(200).json(finalData2);
      })  
  })
}

const dashboardProject  = (request, response) => {
  var resultDataAll = []
  var taskData = []
  var finalData = []
  var finalList = []
  var reason = false
  const id = request.body.id
  const search = '%' + request.body.search + '%'
  var days2 = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  
  pool.query('SELECT r.have_reason AS reason, p.project_name AS projectname, ro.role_name AS rolename, array_agg(t.task_name) AS taskname, array_agg(t.progress) AS taskprogress, r.created_date AS tgl FROM report AS r, projects as p, task AS t, roles AS ro WHERE r.project_id = p.project_id AND r.task_id = t.task_id AND r.role_id = ro.role_id AND r.user_id = $1 AND LOWER(p.project_name) LIKE LOWER($2) GROUP BY r.have_reason, r.created_date, p.project_name, ro.role_name, r.role_id ORDER BY r.created_date ASC, p.project_name ASC',
              [id,search] ,(error, results) => {

      if (error) {
          throw error
      }
      if(results){
        if(results.rows.length > 0){
          var dayName = null
          var tempDate = null
          
          for(let x = 0 ; x<results.rows.length ; x++){
            if((x>0 && x!=results.rows.length-1 && results.rows[x].projectname!=results.rows[x-1].projectname) || (x>0 && results.rows[x].rolename!=results.rows[x-1].rolename) || (x>0 && results.rows[x].tgl!=results.rows[x-1].tgl)){
              resultDataAll.push({
                cardTitle: dayName,
                cardList: {
                  projectName: results.rows[x-1].projectname,
                  listRoles: {
                    roleName: results.rows[x-1].rolename,
                    listTasks: taskData 
                  }
                },
                cardDate: tempDate,
                cardId: id,
                haveReason: reason, 
              })
              taskData = []

            }
        
            taskData.push({
              taskName: results.rows[x].taskname,
              taskProgress: results.rows[x].taskprogress
            })          
            
            var tglTemp = results.rows[x].tgl + ''
            var tglfix = tglTemp.split(' ')
            tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
            var parts1 = tempDate.split('-')
            var date1 = new Date(parts1[0], parts1[1], parts1[2])
            date1.setMonth(date1.getMonth()-1)
            date1.setDate(date1.getDate()+1)
            var dayArray = date1.getDay() - 1
            var monthArray = date1.getMonth()
            if(dayArray < 0){
              dayArray = 6
            }
            if(monthArray < 0){
              monthArray = 11
            }
            reason = results.rows[x].reason
            dayName = days2[dayArray] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[monthArray] + ' ' + tglfix[3]; 
            
            if(x==results.rows.length-1){
              resultDataAll.push({
                cardTitle: dayName,
                cardList: {
                  projectName: results.rows[x].projectname,
                  listRoles: {
                    roleName: results.rows[x].rolename,
                    listTasks: taskData 
                  }
                },
                cardDate: tempDate,
                cardId: id,
                haveReason: reason, 
              })
              taskData = []   
            }
          }

          for(z=0 ; z<resultDataAll.length ; z++){
            if(z>0 && resultDataAll[z].cardTitle!=resultDataAll[z-1].cardTitle){
              finalData.push({
                cardTitle: resultDataAll[z-1].cardTitle,
                cardBody: finalList,
                cardDate: resultDataAll[z-1].cardDate,
                cardId: resultDataAll[z-1].cardId,
                status: "full",
                haveReason: resultDataAll[z-1].haveReason,
              })
              finalList = []
            }
            if(z==resultDataAll.length-1){
              finalList.push({
                projectName: resultDataAll[z].cardList.projectName,
                roleName: resultDataAll[z].cardList.listRoles.roleName,
                taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
                taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
              })
              finalData.push({
                cardTitle: resultDataAll[z].cardTitle,
                cardBody: finalList,
                cardDate: resultDataAll[z].cardDate,
                cardId: resultDataAll[z].cardId,
                status: "full",
                haveReason: resultDataAll[z].haveReason,
              })
              finalList = []
            }
            finalList.push({
              projectName: resultDataAll[z].cardList.projectName,
              roleName: resultDataAll[z].cardList.listRoles.roleName,
              taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
              taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
            })
          }
        } else {
          response.status(200).json(results.rows)
          return
        }
      }

      response.status(200).json(finalData); 
  })
}

const dashboardGroupTime  = (request, response) => {
  var resultDataAll = []
  var taskData = []
  var finalData = []
  var finalList = []
  var reason = false
  var bulanReport = ''
  var tahunReport = ''
  var tglReport = ''
  var tglReportInt = 0
  const id = request.body.id
  var search = request.body.search
  search = search.split('-')
  const bulan = ('0' + (search[0])).slice(-2)
  const tahun = search[1]
  var days2 = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  
  pool.query('SELECT r.have_reason AS reason, p.project_name AS projectname, ro.role_name AS rolename, array_agg(t.task_name) AS taskname, array_agg(t.progress) AS taskprogress, r.created_date AS tgl FROM report AS r, projects as p, task AS t, roles AS ro WHERE r.project_id = p.project_id AND r.task_id = t.task_id AND r.role_id = ro.role_id AND r.user_id = $1 AND EXTRACT(month from r.created_date) = $2 AND EXTRACT(year from r.created_date) = $3 GROUP BY r.have_reason, r.created_date, p.project_name, ro.role_name, r.role_id ORDER BY r.created_date ASC, p.project_name ASC',
              [id,bulan,tahun] ,(error, results) => {
      if (error) {
          throw error
      }
      if(results){
        if(results.rows.length > 0){
          var dayName = null
          var tempDate = null
          
          for(let x = 0 ; x<results.rows.length ; x++){
            if((x>0 && x!=results.rows.length-1 && results.rows[x].projectname!=results.rows[x-1].projectname) || (x>0 && results.rows[x].rolename!=results.rows[x-1].rolename) || (x>0 && results.rows[x].tgl!=results.rows[x-1].tgl)){
              resultDataAll.push({
                cardTitle: dayName,
                cardList: {
                  projectName: results.rows[x-1].projectname,
                  listRoles: {
                    roleName: results.rows[x-1].rolename,
                    listTasks: taskData 
                  }
                },
                cardDate: tempDate,
                cardId: id,
                haveReason: reason, 
              })
              taskData = []

            }
        
            taskData.push({
              taskName: results.rows[x].taskname,
              taskProgress: results.rows[x].taskprogress
            })          
            
            var tglTemp = results.rows[x].tgl + ''
            var tglfix = tglTemp.split(' ')
            tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
            var parts1 = tempDate.split('-')
            var date1 = new Date(parts1[0], parts1[1], parts1[2])
            date1.setMonth(date1.getMonth()-1)
            date1.setDate(date1.getDate()+1)
            var dayArray = date1.getDay() - 1
            var monthArray = date1.getMonth()
            if(dayArray < 0){
              dayArray = 6
            }
            if(monthArray < 0){
              monthArray = 11
            }
            reason = results.rows[x].reason
            if(x==0){
              tahunReport = tglfix[3]
              tglReport = ('0' + (tglfix[2])).slice(-2)
              bulanReport = ('0' + (date1.getMonth()+1)).slice(-2)
              tglReportInt = date1.getDate() - 1
            }
            dayName = days2[dayArray] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[monthArray] + ' ' + tglfix[3]; 
            
            if(x==results.rows.length-1){
              resultDataAll.push({
                cardTitle: dayName,
                cardList: {
                  projectName: results.rows[x].projectname,
                  listRoles: {
                    roleName: results.rows[x].rolename,
                    listTasks: taskData 
                  }
                },
                cardDate: tempDate,
                cardId: id,
                haveReason: reason, 
              })
              taskData = []   
            }
          }

          for(z=0 ; z<resultDataAll.length ; z++){
            if(z>0 && resultDataAll[z].cardTitle!=resultDataAll[z-1].cardTitle){
              finalData.push({
                cardTitle: resultDataAll[z-1].cardTitle,
                cardBody: finalList,
                cardDate: resultDataAll[z-1].cardDate,
                cardId: resultDataAll[z-1].cardId,
                status: "full",
                haveReason: resultDataAll[z-1].haveReason,
              })
              finalList = []
            }
            if(z==resultDataAll.length-1){
              finalList.push({
                projectName: resultDataAll[z].cardList.projectName,
                roleName: resultDataAll[z].cardList.listRoles.roleName,
                taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
                taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
              })
              finalData.push({
                cardTitle: resultDataAll[z].cardTitle,
                cardBody: finalList,
                cardDate: resultDataAll[z].cardDate,
                cardId: resultDataAll[z].cardId,
                status: "full",
                haveReason: resultDataAll[z].haveReason,
              })
              finalList = []
            }
            finalList.push({
              projectName: resultDataAll[z].cardList.projectName,
              roleName: resultDataAll[z].cardList.listRoles.roleName,
              taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
              taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
            })
          }
        } else {
          response.status(200).json(results.rows)
          return
        }
      }
      
      var flagSysdate = false
      var jmlHariAkhir = new Date(tahun, bulan, 0).getDate()
      var sysDate = new Date()
      var tglAkhir = ''
      if((sysDate.getMonth()+1).toString() === bulan && sysDate.getFullYear().toString() === tahun){
        flagSysdate = true
      }
      if(flagSysdate === false){
        tglAkhir = tahun + '-' +  ('0' + (bulan)).slice(-2) + '-' + jmlHariAkhir.toString()
      } else if(flagSysdate === true){
        tglAkhir = tahun + '-' +  ('0' + (bulan)).slice(-2) + '-' + ('0' + (sysDate.getDate())).slice(-2)
      }
      var finalData2 = []

        var tglAwal = ''
        var tglCheck = 0
        if(bulanReport === bulan && tahunReport === tahun && tglReport > '01'){
          tglAwal = tahun + '-' +  ('0' + (bulan)).slice(-2) + '-' + tglReport
          tglCheck = tglReportInt
          console.log(tglReportInt)
        } else {
          tglCheck = 1
          tglAwal = tahun + '-' +  ('0' + (bulan)).slice(-2) + '-' + '01'
        }
        var jmlHari = 0
        var bulanCheck = bulan 
        var tahunCheck = tahun
        var idx = 0
        var flagArray = 0
        var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];

        while(true){
          if(idx == 0){
            jmlHari = new Date(tahunCheck, bulanCheck, 0).getDate()
          } else if (tglCheck > jmlHari && idx > 0){
            tglCheck = 1;
          }
          if(tglCheck == 1 && idx > 0){
            bulanCheck += 1;
            jmlHari = new Date(tahunCheck, bulanCheck, 0).getDate()
          }
          if(bulanCheck > 12 && idx > 0){
            bulanCheck = 1
            tahunCheck += 1
          }

          tglAwal = tahunCheck + '-' + ('0' + (bulanCheck)).slice(-2) + '-' + ('0' + (tglCheck)).slice(-2)
          dateTemp = new Date(tahunCheck, bulanCheck, tglCheck)
          dateTemp.setMonth(dateTemp.getMonth()-1)
          dateTemp.setDate(dateTemp.getDate()+1)
          var dayArray = dateTemp.getDay() - 1
          var monthArray = bulanCheck - 1
          if(dayArray < 0){
            dayArray = 6
          }
          if(monthArray < 0){
            monthArray = 11
          }
          
          
          tglAwalTag = days[dayArray] + ', ' + ('0' + (tglCheck)).slice(-2) + ' ' + months[monthArray] + ' ' + tahunCheck;

          if(flagArray == 99) {
            finalData2.push({
              cardTitle: tglAwalTag,
              cardBody: "",
              cardDate: tglAwal,
              cardId: "",
              status: "empty",
              haveReason: false,
            })
            if(tglAkhir === tglAwal){
              break;
            }
          } else if(flagArray == 0){ 
            if(finalData[idx].cardDate == tglAwal){
              finalData2.push(finalData[idx])
              idx+=1
            } else if (finalData[idx].cardDate != tglAwal){
              if(tglAkhir === tglAwal){
                break;
              }
              finalData2.push({
                cardTitle: tglAwalTag,
                cardBody: "",
                cardDate: tglAwal,
                cardId: "",
                status: "empty",
                haveReason: false,
              })
            }
            if(tglAkhir === tglAwal){
              break;
            } 
          }

          tglCheck+=1
          if(idx >= finalData.length){
            flagArray = 99
          }
        }

        response.status(200).json(finalData2);
  })
}

const dashboardSearchTime  = (request, response) => {
  var resultDataAll = []
  var taskData = []
  var finalData = []
  var finalList = []
  var reason = false
  const id = request.body.id
  var search = request.body.search
  search = search.toString().toLowerCase()
  var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  var monthIdx = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  
  pool.query('SELECT r.have_reason AS reason, p.project_name AS projectname, ro.role_name AS rolename, array_agg(t.task_name) AS taskname, array_agg(t.progress) AS taskprogress, r.created_date AS tgl FROM report AS r, projects as p, task AS t, roles AS ro WHERE r.project_id = p.project_id AND r.task_id = t.task_id AND r.role_id = ro.role_id AND r.user_id = $1 GROUP BY r.have_reason, r.created_date, p.project_name, ro.role_name, r.role_id ORDER BY r.created_date ASC, p.project_name ASC',
              [id] ,(error, results) => {

      if (error) {
          throw error
      }
      if(results){
        if(results.rows.length > 0){
          var dayName = null
          var tempDate = null
          
          for(let x = 0 ; x<results.rows.length ; x++){
            if((x>0 && x!=results.rows.length-1 && results.rows[x].projectname!=results.rows[x-1].projectname) || (x>0 && results.rows[x].rolename!=results.rows[x-1].rolename) || (x>0 && results.rows[x].tgl!=results.rows[x-1].tgl)){
              resultDataAll.push({
                cardTitle: dayName,
                cardList: {
                  projectName: results.rows[x-1].projectname,
                  listRoles: {
                    roleName: results.rows[x-1].rolename,
                    listTasks: taskData 
                  }
                },
                cardDate: tempDate,
                cardId: id,
                haveReason: reason, 
              })
              taskData = []

            }
        
            taskData.push({
              taskName: results.rows[x].taskname,
              taskProgress: results.rows[x].taskprogress
            })          
            
            var tglTemp = results.rows[x].tgl + ''
            var tglfix = tglTemp.split(' ')
            tempDate = tglfix[3] + '-' + ('0' + (monthIdx.indexOf(tglfix[1])+1)).slice(-2) + '-' + ('0' + (tglfix[2])).slice(-2)
            var parts1 = tempDate.split('-')
            var date1 = new Date(parts1[0], parts1[1], parts1[2])
            date1.setMonth(date1.getMonth()-1)
            date1.setDate(date1.getDate()+1)
            reason = results.rows[x].reason
            dayName = days[date1.getDay()-1] + ', ' + ('0' + (tglfix[2])).slice(-2) + ' ' + months[monthIdx.indexOf(tglfix[1])] + ' ' + tglfix[3]; 
            
            if(x==results.rows.length-1){
              resultDataAll.push({
                cardTitle: dayName,
                cardList: {
                  projectName: results.rows[x].projectname,
                  listRoles: {
                    roleName: results.rows[x].rolename,
                    listTasks: taskData 
                  }
                },
                cardDate: tempDate,
                cardId: id,
                haveReason: reason, 
              })
              taskData = []   
            }
          }

          for(z=0 ; z<resultDataAll.length ; z++){
            if(z>0 && resultDataAll[z].cardTitle!=resultDataAll[z-1].cardTitle){
              finalData.push({
                cardTitle: resultDataAll[z-1].cardTitle,
                cardBody: finalList,
                cardDate: resultDataAll[z-1].cardDate,
                cardId: resultDataAll[z-1].cardId,
                status: "full",
                haveReason: resultDataAll[z-1].haveReason,
              })
              finalList = []
            }
            if(z==resultDataAll.length-1){
              finalList.push({
                projectName: resultDataAll[z].cardList.projectName,
                roleName: resultDataAll[z].cardList.listRoles.roleName,
                taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
                taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
              })
              finalData.push({
                cardTitle: resultDataAll[z].cardTitle,
                cardBody: finalList,
                cardDate: resultDataAll[z].cardDate,
                cardId: resultDataAll[z].cardId,
                status: "full",
                haveReason: resultDataAll[z].haveReason,
              })
              finalList = []
            }
            finalList.push({
              projectName: resultDataAll[z].cardList.projectName,
              roleName: resultDataAll[z].cardList.listRoles.roleName,
              taskName: resultDataAll[z].cardList.listRoles.listTasks[0].taskName,
              taskProgress: resultDataAll[z].cardList.listRoles.listTasks[0].taskProgress,
            })
          }
        }
      }
      var tglAkhirTemp = new Date()
      var tglAkhir = tglAkhirTemp.getFullYear() + '-' +  ('0' + (tglAkhirTemp.getMonth()+1)).slice(-2) + '-' + ('0' + (tglAkhirTemp.getDate())).slice(-2)
      var finalData2 = []
      pool.query('SELECT created_date AS tglawal FROM report WHERE user_id = $1 ORDER BY created_date ASC LIMIT 1', [id], (error, results) => {
        if(error){
          throw error
        }

        var tglTemp2 = results.rows[0].tglawal + ''
        var tglfix2 = tglTemp2.split(' ')
        var tglAwal = tglfix2[3] + '-' + ('0' + (monthIdx.indexOf(tglfix2[1])+1)).slice(-2) + '-' + ('0' + (tglfix2[2])).slice(-2)
        var jmlHari = 0
        var tglCheck = Number(tglfix2[2])
        var bulanCheck = monthIdx.indexOf(tglfix2[1]) + 1
        var tahunCheck = Number(tglfix2[3])
        var idx = 0
        var flagArray = 0
        var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];

        while(true){
          if(idx == 0){
            jmlHari = new Date(tahunCheck, bulanCheck, 0).getDate()
          } else if (tglCheck > jmlHari && idx > 0){
            tglCheck = 1;
          }
          if(tglCheck == 1 && idx > 0){
            bulanCheck += 1;
            jmlHari = new Date(tahunCheck, bulanCheck, 0).getDate()
          }
          if(bulanCheck > 12 && idx > 0){
            bulanCheck = 1
            tahunCheck += 1
          }

          tglAwal = tahunCheck + '-' + ('0' + (bulanCheck)).slice(-2) + '-' + ('0' + (tglCheck)).slice(-2)
          dateTemp = new Date(tahunCheck, bulanCheck, tglCheck)
          dateTemp.setMonth(dateTemp.getMonth()-1)
          dateTemp.setDate(dateTemp.getDate()+1)
          var dayArray = dateTemp.getDay() - 1
          var monthArray = bulanCheck - 1
          if(dayArray < 0){
            dayArray = 6
          }
          if(monthArray < 0){
            monthArray = 11
          }
          
          
          tglAwalTag = days[dayArray] + ', ' + ('0' + (tglCheck)).slice(-2) + ' ' + months[monthArray] + ' ' + tahunCheck;

          if(flagArray == 99) {
            finalData2.push({
              cardTitle: tglAwalTag,
              cardBody: "",
              cardDate: tglAwal,
              cardId: "",
              status: "empty",
              haveReason: false,
            })
            if(tglAkhir === tglAwal){
              break;
            }
          } else if(flagArray == 0){ 
            if(finalData[idx].cardDate == tglAwal){
              finalData2.push(finalData[idx])
              idx+=1
            } else if (finalData[idx].cardDate != tglAwal){
              if(tglAkhir === tglAwal){
                break;
              }
              finalData2.push({
                cardTitle: tglAwalTag,
                cardBody: "",
                cardDate: tglAwal,
                cardId: "",
                status: "empty",
                haveReason: false,
              })
            }
            if(tglAkhir === tglAwal){
              break;
            } 
          }

          tglCheck+=1
          if(idx >= finalData.length){
            flagArray = 99
          }
        }

        response.status(200).json(finalData2);
      })  
  })
}

const totalUserTasks = (request, response) => {
  const userId = request.body.userId
  const projectId = request.body.projectId
  pool.query('SELECT COUNT(task_id) AS total FROM report WHERE user_id = $1 AND project_id = $2', [userId,projectId], (error, results) => {
      if (error) {
          throw error
      }
      response.status(200).json(results.rows[0])
  })
}

const reportDetailUser = (request, response) => {
  const userId = request.body.userId
  const created_date = request.body.created_date

  pool.query('select u.user_id, p.project_name, ro.role_id, ro.role_name, p.project_id, COUNT(t.task_id) as total_task, r.created_date from report r, roles ro, users u , projects p, task t where u.user_id = r.user_id AND ro.role_id = r.role_id AND t.task_id = r.task_id AND r.project_id = p.project_id AND r.user_id = $1 AND r.created_date = $2 GROUP BY u.user_id, p.project_name, ro.role_id, p.project_id, r.created_date', [userId, created_date],  (error, results) => {
      if(error){
        return error;
      }
      if(results){
          // data.push({
          //     result: results.rows,
          //     project_id : projectId,
          //     role_id : results.rows.roleID
          //     })
          response.status(200).send(results.rows)
          console.log("ini result detail => ",results.rows)
      }
  })
}

const taskListUser = (request, response) => {
  const roleId = request.body.roleId
  const projectId = request.body.projectId
  const userId = request.body.userId
  const createdDate = request.body.created_date

  console.log("ID role => ",roleId)
  console.log("ID project =>",projectId)
  console.log("ID role => ", userId)
  console.log("ID createdDate =>", createdDate)

  pool.query('select t.task_id, t.task_name, t.task_description, t.progress from report r, task t where r.project_id = $1 AND r.user_id = $2 AND r.role_id = $3 AND r.task_id = t.task_id AND r.created_date = $4', [projectId, userId, roleId, createdDate], (error, results) => {
      if (error) {
          throw error
      }
      if (results) {
          console.log("ini result detail => ", results.rows)
          response.status(200).send(results.rows)
      }
  })
}

const forgotPassword = (request, response) => {
  const email = request.body.email

  pool.query('SELECT COUNT(*) AS jumlah FROM users WHERE email = $1', [email], (error, results) => {
    if (error) {
        response.status(403).send('Network/Server Error!')
        return;
    } else {
      console.log('test masuk')
      if(results.rows[0].jumlah == 0){
        return response.status(404).send('There are no user registered with this email address!')
      }
      else if(results.rows[0].jumlah == 1){
        var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                retVal += charset.charAt(Math.floor(Math.random() * n));
            }
    
            //Proses kirim ke email
          let transporter = nodeMailer.createTransport({
              host: 'smtp.gmail.com',
              port: 465,
              secure: true,
              auth: {
                  user: 'tematik19@gmail.com',
                  pass: 'iniuntuktemat1k'
              }
          });
          let mailOptions = {
              from: '"Tematik" <tematik19@gmail.com>', // sender address
              to: email, // list of receivers
              subject: "Forgot Password Confirmation - ProgressTracker", // Subject line
              text: "ini password baru anda: " + retVal, // plain text body
              html: 'Ini password baru anda: ' + retVal + "<hr/>" + 'Jangan bagikan password ini ke siapapun. Terima Kasih.' // html body
          };
    
          console.log("password => ", mailOptions.text);
          transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                  return console.log(error);
              }
              console.log('Message %s sent: %s', info.messageId, info.response);
                  res.render('index');
              });
    
        var hash = crypto.createHash('md5').update(retVal).digest('hex');
        console.log(hash);
    
        pool.query('UPDATE users SET password = $1 WHERE email = $2', [hash,email], (error, results) => {
            if (error) {
                response.status(403).send(error.detail)
                return;
            }
            response.status(201).send('PASSWORD SENT!')
        })
      }
    }
  })
}


// const getReport = (request, response) => {

//   pool.query('select u.user_id, p.project_name, ro.role_id, p.project_id, COUNT(t.task_id) as total_task,  r.created_date from report r, roles ro, users u , projects p, task t where u.user_id = r.user_id AND ro.role_id = r.role_id AND t.task_id = r.task_id AND r.project_id = p.project_id AND r.user_id = $1 AND r.created_date = $2 GROUP BY u.user_id, p.project_name, ro.role_id, p.project_id, r.created_date')


// }

 const insertReport = async (request,response) => {
  
  var object = request.body.objectLaporan
  
  // var created_date = new Date()
  var created_date = request.body.created_date
  
  var user_id = request.body.userId 
  
  var task_id = [];
  
  var berhenti = [];
  
  var counter = 0;

  var have_reason = false;

   for(i=0; i<object.length; i++) {
    console.log("MASUK LOOPING")
    var object2 = object[i].objectTask
    
    var role_id = object[i].roleId
    
    var project_id = object[i].projectId
    
    for(j=0; j<object2.length; j++) {
      console.log("MASUK LOOPING KEDUA")
        var task_name = object2[j].taskName
        
        var task_description = object2[j].taskDescription
        
        var progress = object2[j].progressTask
        
        var jumlah_task = [];
        
          jumlah_task = await pool.query('INSERT INTO task(task_name, task_description, progress) VALUES($1,$2,$3) returning task_id',[task_name, task_description, progress])

        // let results = await pool.query('select * from task ORDER BY task_id DESC ')

         task_id = jumlah_task.rows;
         console.log("TaskId => ", task_id);
         
          console.log("Masukkan data ke tabel REPORT")

           console.log(task_id[0].task_id);
          await pool.query('INSERT INTO report(user_id, role_id, project_id, task_id, created_date, have_reason) VALUES($1,$2,$3,$4,$5,$6)', [user_id, role_id, project_id, task_id[0].task_id, created_date, have_reason])

    }
  }
  return response.send({"message": 'Insert success !', code : 201})
}

const editReportUser = async (request, response) => {

  console.log("MASUK INSERT REPORT")
  const object = request.body.objectLaporan
  // console.log("object => ", object)
  const created_date = request.body.created_date
  // console.log("created date => ", created_date)
  const user_id = request.body.userId
  //nanti minta frendy ambil userID dari session storage, trus kirim ke sini
  // console.log("user id => ", user_id)

  console.log("SEBELUM LOOPING")

  for(i=0; i<object.length; i++) {
    // console.log("MASUK LOOPING")
    const object2 = object[i].objectTask
    // console.log("object", object2)
    const role_id = object[i].roleId
    // console.log("role id => ", role_id)
    const project_id = object[i].projectId
    // console.log("project id => ", project_id)

    for(j=0; j<object2.length; j++) {
      console.log("MASUK LOOPING KEDUA")
      const task_id = object2[j].taskId
      // console.log("task id => ", task_id)
        const task_name = object2[j].taskName
        // console.log("task name => ", task_name)
        const task_description = object2[j].taskDescription
        // console.log("task description => ", task_description)
        const progress = object2[j].progressTask
        // console.log("task progress => ", progress)

        // console.log("masukin data ke tabel TASK")
         await pool.query('UPDATE task SET task_name = $1, task_description = $2, progress = $3 WHERE task_id = $4',[task_name, task_description, progress, task_id])

          console.log("Masukkan data ke tabel REPORT")
          await pool.query('UPDATE report SET created_date = $1 where task_id = $2 AND user_id = $3', [created_date, task_id, user_id])
          console.log("SELESAI DARI TABEL REPORT")
    }
  }
  return response.send({"message": 'Edit success !', code: 201})
}

const setReason = (request, response) => {
  const id = request.body.id
  const reportDate = request.body.reportDate

  pool.query('INSERT INTO report(user_id, role_id, project_id, task_id, created_date, have_reason) VALUES($1,0,0,0,$2,true)', [id,reportDate], (error, results) => {
      if(error){
        response.status(200).json({"code": 201, "message": "failed", "error": error})
      }
      response.status(200).json({"code": 200, "message": "success"})
  })
}

const countUserTasks = (request, response) => {
  const userId = request.body.userId
  const projectId = request.body.projectId
  const roleId = request.body.roleId

  pool.query('SELECT COUNT(*) AS jumlah FROM report WHERE user_id = $1 AND project_id = $2 AND role_id = $3', [userId,projectId,roleId], (error, results) => {
      if(error){
        response.status(200).json({"code": 201, "message": "failed", "error": error})
      }
      response.status(200).json({"code": 200, "message": "success", "taskCount": results.rows[0].jumlah})
  })
}

module.exports = {
    getAllRoles,
    getAllProjects,
    insertReport,
    dashboardAll,
    login,
    reportDetailUser,
    taskListUser,
    dashboardProject,
    dashboardGroupTime,
    dashboardSearchTime,
    totalUserTasks,
    editReportUser,
    forgotPassword,
    setReason,
    countUserTasks,
}