const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const dbAdmin = require('./apiAdmin');
const dbUser = require('./apiUser');

const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  next();
});
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

app.get('/', (request, response) => {
  response.json({info: 'Node.js, Express, and Postgres API'});
});

// ENDPOINT ADMIN
app.get('/admin/listRolesDashboard', cors(), dbAdmin.adminShowListRoles);
app.get('/admin/listUsersDashboard', cors(), dbAdmin.adminShowListUsers);
app.get('/admin/listProjectsDashboard', cors(), dbAdmin.adminShowListProjects);
app.post('/admin/deleteUser/', cors(), dbAdmin.adminDeleteUser);
app.post('/admin/deleteRole/', cors(), dbAdmin.adminDeleteRole);
app.post('/admin/deleteProject/', cors(), dbAdmin.adminDeleteProject);
app.post('/admin/editUser/', cors(), dbAdmin.adminEditUser);
app.post('/admin/editRole/', cors(), dbAdmin.adminEditRole);
app.post('/admin/editProject/', cors(), dbAdmin.adminEditProject);
app.post('/admin/addProject', cors(), dbAdmin.adminAddProject);
app.post('/admin/addRole', cors(), dbAdmin.adminAddRole);
app.post('/admin/addUser', cors(), dbAdmin.adminAddUser);
app.post('/admin/searchProject', cors(), dbAdmin.adminSearchProject);
app.post('/admin/searchRole', cors(), dbAdmin.adminSearchRole);
app.post('/admin/searchUser', cors(), dbAdmin.adminSearchUser);
app.post('/admin/project', cors(), dbAdmin.adminDetailProject1);
app.post('/admin/role', cors(), dbAdmin.adminDetailRole1);
app.post('/admin/user', cors(), dbAdmin.adminDetailUser1);
app.post('/admin/projectAll', cors(), dbAdmin.adminDetailProject2);
app.post('/admin/roleAll', cors(), dbAdmin.adminDetailRole2);
app.post('/admin/userAll', cors(), dbAdmin.adminDetailUser2);
app.post('/admin', cors(), dbAdmin.login);
app.post('/admin/detailByproject', cors(), dbAdmin.reportDetailByProject);
app.post('/admin/detailByuser', cors(), dbAdmin.reportDetailByUser);
// app.post('/admin/detailByProject/taskByProject', cors(), dbAdmin.taskList);
app.post('/admin/detailByrole', cors(), dbAdmin.reportDetailByRole);
app.post('/admin/taskList', cors(), dbAdmin.taskList);
app.post('/admin/deleteReportuser', cors(), dbAdmin.deleteReport1);
app.post('/admin/deleteReportproject', cors(), dbAdmin.deleteReport2);
app.post('/admin/deleteReportrole', cors(), dbAdmin.deleteReport3);

// ENDPOINT USER
app.post('/login', cors(), dbUser.login);
app.get('/user/getAllRoles', cors(), dbUser.getAllRoles);
app.get('/user/getAllProjects', cors(), dbUser.getAllProjects);
app.post('/user/insertReport', cors(), dbUser.insertReport);
app.post('/user/dashboardAll', cors(), dbUser.dashboardAll);
app.post('/user/reportDetail', cors(), dbUser.reportDetailUser);
app.post('/user/reportDetail/taskList', cors(), dbUser.taskListUser);
app.post('/user/dashboardSearchProject', cors(), dbUser.dashboardProject);
app.post('/user/dashboardGroupTime', cors(), dbUser.dashboardGroupTime);
app.post('/user/dashboardSearchTime', cors(), dbUser.dashboardSearchTime);
app.post('/user/totalUserTasks', cors(), dbUser.totalUserTasks);
app.post('/user/editReportUser', cors(), dbUser.editReportUser);
app.post('/user/forgetPassword', cors(), dbUser.forgotPassword);
app.post('/user/addReason', cors(), dbUser.setReason);
app.post('/user/countTask', cors(), dbUser.countUserTasks);


app.listen(process.env.PORT || 3000, function() {
  console.log(
    'Express server listening on port %d in %s mode',
    this.address().port,
    app.settings.env,
  );
});
